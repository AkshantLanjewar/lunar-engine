#include "../../public/LunarUltralight/UltralightManager.h"

namespace LunarGUI {
	void UltralightManager::RenderViews() {
		renderer->Update();
		renderer->Render();
		UpdateUBOS();

		for (auto layout : layouts) {
			if (layout.active) {
				for (auto pane : layout._panes) {
					ultralight::BitmapSurface* bitmap_surface = (ultralight::BitmapSurface*)pane._view->get()->surface();
					ultralight::RefPtr<ultralight::Bitmap> bmp = bitmap_surface->bitmap();
					bmp->SwapRedBlueChannels();

					void* pixels = bmp->LockPixels();

					VkDeviceSize imageSize = bmp->size();

					void* data;
					vkMapMemory(device, pane.imageStagingBufferMemory, 0, imageSize, 0, &data);
					memcpy(data, pixels, static_cast<uint32_t>(imageSize));
					vkUnmapMemory(device, pane.imageStagingBufferMemory);

					bmp->UnlockPixels();
				}
			}
		} 
	}

	void UltralightManager::RecreateDims(int width, int height) {
		glm::mat4 projection = glm::ortho(0.0f, (float)swapChainExtent.width, (float)swapChainExtent.height, 0.0f, 0.1f, 100.0f);

		for (auto layout : layouts) {
			for (auto pane : layout._panes) {
				pane.ubo.projection = projection;
			}
		}

		UpdateUBOS();
	}

	void UltralightManager::UpdateUBOS() {
		for (auto layout : layouts) {
			if (layout.active) {
				for (auto pane : layout._panes) {
					void* data;
					vkMapMemory(device, pane.uniformBuffersMemory[0], 0, sizeof(pane.ubo), 0, &data);
						memcpy(data, &pane.ubo, sizeof(pane.ubo));
					vkUnmapMemory(device, pane.uniformBuffersMemory[0]);
				}
			}
		}
	}

	void UltralightManager::FlushCommandBuffer(VkCommandBuffer& commandBuffer) {
		//copy buffer
		for (auto layout : layouts) {
			if (layout.active) {
				for (auto pane : layout._panes) {
					

					transitionImageLayout(pane.textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
						VkBufferImageCopy region = {};
						region.bufferOffset = 0;
						region.bufferRowLength = 0;
						region.bufferImageHeight = 0;

						region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
						region.imageSubresource.mipLevel = 0;
						region.imageSubresource.baseArrayLayer = 0;
						region.imageSubresource.layerCount = 1;

						region.imageOffset = { 0, 0, 0 };
						region.imageExtent = {
							static_cast<uint32_t>(pane.paneWidth), static_cast<uint32_t>(pane.paneHeight), 1
						};

						vkCmdCopyBufferToImage(commandBuffer, pane.imageStagingBuffer, pane.textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
					transitionImageLayout(pane.textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
				}
			}
		}

		for (auto layout : layouts) {
			if (layout.active) {
				VkDeviceSize offsets[] = { 0 };

				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &layout.vertexBuffer, offsets);
				vkCmdBindIndexBuffer(commandBuffer, layout.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
				for (auto pane : layout._panes) {
					VkPipeline pipe = ultralightPipeline;
					VkPipelineLayout layout = ultralightPipelineLayout;

					vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipe);
					vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &pane.descriptorSet, 0, nullptr);
					vkCmdDrawIndexed(commandBuffer, static_cast<uint32_t>(pane.indices.size()), 1, 0, pane.indexBase, 0);
				}
			}
		}
	}

	void UltralightManager::RenderDT(float deltaTime) {
		currAccumulator += deltaTime;
		if (currAccumulator >= renderTime) {
			currAccumulator = 0;
			RenderViews();
		}
	}
}